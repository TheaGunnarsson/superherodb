Use SuperheroesDB;

INSERT INTO Superhero(Id, Name, Alias, Origin)
VALUES(1, 'Tony Stark', 'Iron Man', 'SomePlace'),
	  (2, 'Peter Quill', 'Star-Lord', 'Space'),
	  (3, 'Natasha Romanoff', 'Black Widow', 'Russia');