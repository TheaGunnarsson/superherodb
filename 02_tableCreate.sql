Use SuperheroesDB;

CREATE TABLE Superhero (
	Id int NOT NULL,
	Name varchar(50),
	Alias varchar(50),
	Origin varchar(50),
	PRIMARY KEY (Id)
);

CREATE TABLE Assistant (
	Id int NOT NULL,
	Name varchar(50),
	PRIMARY KEY (Id)
);

CREATE TABLE Power (
	Id int NOT NULL,
	Name varchar(50),
	Description varchar(255),
	PRIMARY KEY (Id)
);