Use SuperheroesDB;

INSERT INTO Power (Id, Name, Description)
VALUES(1, 'Intelligence', 'Mega-mind'),
	  (2, 'Fly', 'In the sky, not the bug'),
	  (3, 'Strenght', 'Like bear, in the forest'),
	  (4, 'Speed', 'Wiiiihiiooommm');